<?php 
// Change number or products per row to 5
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 5; // 5 products per row
	}
}
//NUMBER OF PRODUCTS TO DISPLAY ON SHOP PAGE
add_filter( 'loop_shop_per_page',  create_function( '$cols', 'return 24;' ), 20 );
?>